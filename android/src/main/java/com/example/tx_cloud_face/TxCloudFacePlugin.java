package com.example.tx_cloud_face;

import android.app.Activity;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

import com.tencent.cloud.huiyansdkface.facelight.api.WbCloudFaceVerifySdk;
import com.example.tx_cloud_face.wbcloud.WbCloudFaceVerifyKit;

/** TxCloudFacePlugin */
public class TxCloudFacePlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  private Activity mContext;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "tx_cloud_face");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("getPlatformVersion")) {
      WbCloudFaceVerifySdk.InputData inputData = WbCloudFaceVerifyKit.initWbCloudFaceInputData(call.arguments());
      WbCloudFaceVerifyKit.openCloudFaceService(mContext, inputData, (res) -> {
        JSONObject data = new JSONObject();
        try {
          data.put("result", res.isResult());
          data.put("message", res.getMessage());
        } catch (JSONException e) {
          e.printStackTrace();
        }
        result.success(data.toString());
      });
//      result.success("Android666 " + android.os.Build.VERSION.RELEASE);
    } else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  @Override
  public void onAttachedToActivity(@NonNull ActivityPluginBinding activityPluginBinding) {
    mContext = activityPluginBinding.getActivity();
  }
  @Override
  public void onDetachedFromActivityForConfigChanges() {
    this.onDetachedFromActivity();
  }
  @Override
  public void onReattachedToActivityForConfigChanges(ActivityPluginBinding binding) {
    this.onAttachedToActivity(binding);
  }
  @Override
  public void onDetachedFromActivity() {
    mContext = null;
  }
}
