package com.example.tx_cloud_face.wbcloud;

public interface WbCloudFaceVerifyResultListener {
    void onVerifyResultListener(WbCloudFaceVerifyResult result);
}
