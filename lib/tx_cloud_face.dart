import 'dart:convert';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:tx_cloud_face/face_verify_model/wb_cloud_face_params.dart';
import 'package:tx_cloud_face/face_verify_model/wb_cloud_face_verify_result.dart';

class TxCloudFace {
  static const MethodChannel _channel = MethodChannel('tx_cloud_face');

  static Future<WbCloudFaceVerifyResult> openCloudFaceService({
    required WbCloudFaceParams params,
  }) async {
    // final res = await _channel.invokeMethod(
    //     'openCloudFaceService', params.toJson());
    // return WbCloudFaceVerifyResult.fromJson(json.decode(res));
    final res = await _channel.invokeMethod(
        'getPlatformVersion', params.toJson());
    return WbCloudFaceVerifyResult.fromJson(json.decode(res));
    // final String? version = await _channel.invokeMethod('getPlatformVersion');
    // return version;
  }
}
